#!/bin/sh
#
# Install script for Lurker inside a Docker container.
#

BUILD_PACKAGES="
	make
"

PACKAGES="
	rt4-fcgi
	rt4-db-mysql
	request-tracker4
	spawn-fcgi
	multiwatch
	libapache2-mod-fcgid
	libapache2-mod-sso

	spamc
	libhtml-formatexternal-perl
	w3m
	dirmngr
"

APACHE_MODULES_ENABLE="
	fcgid
	sso
	unique_id
"

install_packages() {
    env DEBIAN_FRONTEND=noninteractive apt-get install -qy -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" --no-install-recommends "$@"
}

die() {
    echo "ERROR: $*" >&2
    exit 1
}

set -x

# Install packages.
apt-get -q update
install_packages ${BUILD_PACKAGES} ${PACKAGES} \
    || die "could not install packages"

# Install some useful RT extensions.
env http_proxy= perl -MCPAN -e 'install RT::Extension::ExtractCustomFieldValues ' </dev/null
env http_proxy= perl -MCPAN -e 'install RT::Extension::CommandByMail ' </dev/null

# Enable the apache modules we need, and our rt4-fcgi config.
a2enmod -q ${APACHE_MODULES_ENABLE}
a2enconf -q rt4-fcgi

# Make apache listen on the APACHE_API_PORT too.
echo 'Listen ${APACHE_API_PORT}' >> /etc/apache2/ports.conf
echo 'export APACHE_API_PORT=${APACHE_API_PORT:-8081}' >> /etc/apache2/envvars

# No need to enable the default apache vhost.

# Create mountpoint dirs.
mkdir -p /data /config

# Wipe the RT_SiteConfig.d directory and replace it with
# a link to /config (mountpoint).
rm -fr /etc/request-tracker4/RT_SiteConfig.d
ln -s /config /etc/request-tracker4/RT_SiteConfig.d
ln -s /config/sendmail.json /etc/sendmail.json

# Link back the cache directory to /data (needs to be
# writable by RT).
rm -fr /var/cache/request-tracker4
ln -s /data/cache /var/cache/request-tracker4

# Configure RT logging, for convenience. Everything else
# is loaded from /config at runtime.
cat >/etc/request-tracker4/RT_SiteConfig.pm <<EOF

Set(\$LogToFile, undef);
Set(\$LogToSyslog, 'warning');
Set(\$LogToSTDERR, undef);
Set(\$MailCommand, 'sendmailpipe');
Set(\$SendmailPath, '/usr/sbin/sendmail');
Set(\$SendmailArgs, '-oi -t');
Set(\$ShowRTPortal, 0);
EOF
chmod 0644 /etc/request-tracker4/RT_SiteConfig.pm

chmod 1777 /run

# Clean up.
#apt-get --purge remove ${BUILD_PACKAGES}
apt-get autoremove -y
apt-get clean
rm -fr /var/lib/apt/lists/*
