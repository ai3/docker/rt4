# Stage 1: import the smtpd-pipe server
FROM registry.git.autistici.org/ai3/tools/smtpd-pipe:master AS smtpd

# Stage 2: import the sendmail tool
FROM registry.git.autistici.org/ai3/tools/sendmail-go:master AS sendmail

# Stage 2: build the final container
FROM registry.git.autistici.org/ai3/docker/apache2-base:bookworm
COPY build.sh /tmp/build.sh
COPY conf/ /etc/
RUN /tmp/build.sh && rm /tmp/build.sh
COPY --from=smtpd /smtpd /usr/bin/smtpd
COPY --from=sendmail /sendmail /usr/sbin/sendmail
COPY htdocs/ /var/www/html/

