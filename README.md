rt4
===

Docker image for [RT - Request Tracker](https://bestpractical.com/) (v4).

## SSO

To configure SSO, enable libapache2-mod-sso and set both `$WebRemoteUserAuth`
and `$WebRemoteUserContinuous` to true in the rt4 configuration.

Provide a session key in /config/sso_session_key

## Sending / receiving email

To receive email, configure a postfix-out transport ("rt4") with a destination
of `smtp:[rt4.investici.org]:3025`.

The outbound email configuration must be shipped to /etc/sendmail.json, and
it can be as simple as:

```json
{"smtp_server": "mail-frontend.investici.org:10025"}
```

## Mounts

You should mount /var/lib/rt4 (or whatever directory you like to use for
storage) as /data, and a configuration directory in /config. The configuration
directory should contain configuration snippets (with a `.pm` extension).
The data directory should be writable by the user running the container.

Configure the on-disk external storage with:

```perl
Set(%ExternalStorage,
    Type => 'Disk',
    Path => '/data',
);
```

## GPG

Create a GPG key for the email sender (*help@autistici.org*, for instance),
save it into a keyring in */config/gpg*, and add the configuration:

```perl
Set(%Crypt,
    Incoming => ['GnuPG'],
    Outgoing => 'GnuPG',
);
Set(%GnuPG,
    Enable => 1,
    OutgoingMessagesFormat => 'RFC',
);
Set(%GnuPGOptions,
    'homedir' => '/config/gpg',
    'digest-algo' => 'SHA256',
);
```

## Environment vars

The Docker image sets up a HTTP and a SMTP server. Configure them
with the following environment options:

`APACHE_PORT`: port for the HTTP server (SSO-protected). A Prometheus
exporter will run on APACHE_PORT + 100.

`APACHE_API_PORT`: port for the internal (unauthenticated) HTTP server
exposing the email gateway API. This should not be exposed to the
public.

`SMTP_PORT`: port for the SMTP server (rt-mailer).

`DOMAIN`: domain for the HTTP site

